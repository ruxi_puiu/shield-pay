ShieldPay async project

I've chosen to use Terraform to deploy the infrastructure because I have more experience with this and I thought I can achieve more in less time.
The structure I used for the tf config files allows us to easily deploy the service in a new region if needed.

To build packages:

- yarn
- yarn build

To deploy:

- set AWS_ACCESS_KEY_ID & AWS_SECRET_ACCESS_KEY env vars
- install Terraform cli (https://learn.hashicorp.com/tutorials/terraform/install-cli)
- cd terraform/main
- terraform init
- terraform apply

To run tests:

- yarn test
