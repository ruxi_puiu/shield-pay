export type MessageInputType = {
  phoneNumber: string,
  messageBody: string
}