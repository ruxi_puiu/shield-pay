import { MessageInputType } from '../types';
import { handler } from '../';
import { sns } from '../sns';


describe("Lambda handler", ()=> {
  const validInputMessage: MessageInputType = {
    phoneNumber: "+44343242342342",
    messageBody:"Hello "
  };
  const invalidInputMessage: MessageInputType = {
    phoneNumber: "+44sdsdasa",
    messageBody:"Hello again"
  };
  const snsInvalidMessage = {
    Message: JSON.stringify(invalidInputMessage)
  } as unknown as AWSLambda.SNSMessage
  const snsValidMessage = {
    Message: JSON.stringify(validInputMessage)
  } as unknown as AWSLambda.SNSMessage
  const sqsEvent = {
    Records: [{
      body: JSON.stringify(snsInvalidMessage)
    }, {
      body: JSON.stringify(snsValidMessage)
    }]
  } as unknown as AWSLambda.SQSEvent
  const snsPublishSpy = jest.spyOn(sns, 'publish')
    .mockReturnValue({promise: jest.fn().mockResolvedValue({})} as any);;

  beforeEach(() => {
    jest.clearAllMocks();
  });
  
  it('should not process invalid input', async () => {
    await handler(sqsEvent);

    expect(snsPublishSpy).toHaveBeenCalledTimes(1);
  });

  it('should send valid messages to sns', async () => {
    await handler(sqsEvent);

    expect(snsPublishSpy).toHaveBeenCalledWith({
      Message: validInputMessage.messageBody,
      PhoneNumber: validInputMessage.phoneNumber
    });
  });
});