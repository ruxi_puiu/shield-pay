import * as Joi from 'joi';

export const inputMessageSchema = Joi.object({
  phoneNumber: Joi.string().pattern(/^\+[0-9]+$/).required(),
  messageBody: Joi.string().required()
});