import { inputMessageSchema } from './schema';
import { sns } from './sns';
import { MessageInputType } from './types';

export const handler = async (event: AWSLambda.SQSEvent): Promise<void> => {
  
    const messages = event.Records.map(
      ({ body }: AWSLambda.SQSRecord) => JSON.parse(body) as AWSLambda.SNSMessage
    );
    const validMessages = messages.filter((message) => {
      const { error } = inputMessageSchema.validate(JSON.parse(message.Message));
      if(error) {
        console.log("Some messages failed validation.");
        return false;
      }
      return true;
    });

    const publishTextPromises = validMessages.map((message) => {
      const inputMessage = JSON.parse(message.Message) as MessageInputType;
      return sns.publish({
        Message: inputMessage.messageBody,
        PhoneNumber: inputMessage.phoneNumber,
      }).promise();
    });
    await Promise.allSettled(publishTextPromises);
    console.log("SMS published");
  };