resource "aws_api_gateway_rest_api" "shield_pay_api" {
  name = "${local.namespace}-shiled-pay-api"
}

resource "aws_api_gateway_resource" "shield_pay_api_resource" {
  rest_api_id = aws_api_gateway_rest_api.shield_pay_api.id
  parent_id   = aws_api_gateway_rest_api.shield_pay_api.root_resource_id
  path_part   = "sms"
}

resource "aws_api_gateway_method" "shield_pay_api_method" {
  rest_api_id   = aws_api_gateway_rest_api.shield_pay_api.id
  resource_id   = aws_api_gateway_resource.shield_pay_api_resource.id
  http_method   = "POST"
  authorization = "NONE"
}

resource "aws_api_gateway_integration" "shield_pay_api_integration" {
  rest_api_id             = aws_api_gateway_rest_api.shield_pay_api.id
  resource_id             = aws_api_gateway_resource.shield_pay_api_resource.id
  http_method             = aws_api_gateway_method.shield_pay_api_method.http_method
  type                    = "AWS"
  integration_http_method = "POST"
  uri                     = "arn:aws:apigateway:eu-central-1:sns:path/https://sns.eu-central-1.amazonaws.com/?Action=Publish&TopicArn=${aws_sns_topic.shield_pay_api.arn}"
  credentials             = aws_iam_role.shield_pay_api_role.arn

  request_parameters = {
    "integration.request.querystring.Message" = "method.request.body"
  }
  depends_on = [aws_sns_topic.shield_pay_api]
}

resource "aws_api_gateway_method_response" "response_200" {
  rest_api_id = aws_api_gateway_rest_api.shield_pay_api.id
  resource_id = aws_api_gateway_resource.shield_pay_api_resource.id
  http_method = aws_api_gateway_method.shield_pay_api_method.http_method
  status_code = "200"
}

resource "aws_api_gateway_integration_response" "shield_pay_api_integration_response" {
  rest_api_id = aws_api_gateway_rest_api.shield_pay_api.id
  resource_id = aws_api_gateway_resource.shield_pay_api_resource.id
  http_method = aws_api_gateway_method.shield_pay_api_method.http_method
  status_code = aws_api_gateway_method_response.response_200.status_code
}

resource "aws_api_gateway_deployment" "api_deployment" {
  rest_api_id = aws_api_gateway_rest_api.shield_pay_api.id

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_api_gateway_stage" "api_stage" {
  deployment_id = aws_api_gateway_deployment.api_deployment.id
  rest_api_id   = aws_api_gateway_rest_api.shield_pay_api.id
  stage_name    = "stage"
}
