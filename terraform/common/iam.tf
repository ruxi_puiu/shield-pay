data "aws_iam_policy_document" "shield_pay_api" {
  statement {
    actions = ["sts:AssumeRole"]
    principals {
      type        = "Service"
      identifiers = ["apigateway.amazonaws.com"]
    }
  }
}

resource "aws_iam_role" "shield_pay_api_role" {
  name = "${local.namespace}-role"

  assume_role_policy = data.aws_iam_policy_document.shield_pay_api.json
}

data "aws_iam_policy_document" "shield_api_policy_doc" {
  statement {
    actions = [
      "sns:Publish"
    ]
    resources = [
      aws_sns_topic.shield_pay_api.arn
    ]
  }
}

resource "aws_iam_policy" "shield_api_policy" {
  name = "${local.namespace}-api-policy"
  path = "/"

  policy = data.aws_iam_policy_document.shield_api_policy_doc.json
}

resource "aws_iam_role_policy_attachment" "shield_api_policy_attachment" {
  role       = aws_iam_role.shield_pay_api_role.name
  policy_arn = aws_iam_policy.shield_api_policy.arn
}

data "aws_iam_policy_document" "shield_sqs_policy_doc" {
  statement {
    principals {
      type        = "Service"
      identifiers = ["sns.amazonaws.com"]
    }
    actions = [
      "sqs:SendMessage"
    ]
    resources = [
      aws_sqs_queue.shield_pay_queue.arn
    ]
    condition {
      test = "ArnEquals"

      values = [
        aws_sns_topic.shield_pay_api.arn
      ]

      variable = "aws:SourceArn"
    }
  }
}

resource "aws_sqs_queue_policy" "shield_pay_queue_policy" {
  queue_url = aws_sqs_queue.shield_pay_queue.id

  policy = data.aws_iam_policy_document.shield_sqs_policy_doc.json
}

data "aws_iam_policy_document" "lambda" {
  statement {
    actions = ["sts:AssumeRole"]
    principals {
      type        = "Service"
      identifiers = ["lambda.amazonaws.com"]
    }
  }
}

resource "aws_iam_role" "lambda_role" {
  name = "${local.namespace}-lambda"

  assume_role_policy = data.aws_iam_policy_document.lambda.json
}

resource "aws_iam_policy" "sqs_consumer_policy" {
  name = "${local.namespace}_lambda-sqs-consumer-policy-global"

  policy = data.aws_iam_policy_document.sqs_consumer_global.json
}


data "aws_iam_policy_document" "sqs_consumer_global" {
  statement {
    actions = [
      "sqs:ReceiveMessage",
      "sqs:DeleteMessage",
      "sqs:GetQueueAttributes"
    ]

    resources = [
      aws_sqs_queue.shield_pay_queue.arn
    ]
  }
}

resource "aws_iam_role_policy_attachment" "lambda_policy_attachment" {
  role       = aws_iam_role.lambda_role.name
  policy_arn = aws_iam_policy.sqs_consumer_policy.arn
}

resource "aws_cloudwatch_log_group" "lambda_log_group" {
  name              = "/aws/lambda/${local.namespace}-function"
  retention_in_days = 7
}

data "aws_iam_policy_document" "logging" {
  statement {
    actions = [
      "logs:CreateLogGroup",
      "logs:CreateLogStream",
      "logs:PutLogEvents"
    ]
    resources = [
      "arn:aws:logs:*:*:*"
    ]
  }
}

resource "aws_iam_policy" "lambda_logging" {
  name = "${local.namespace}-function-logging-policy"
  path = "/"

  policy = data.aws_iam_policy_document.logging.json
}

resource "aws_iam_role_policy_attachment" "lambda_logs" {
  role       = aws_iam_role.lambda_role.name
  policy_arn = aws_iam_policy.lambda_logging.arn
}

data "aws_iam_policy_document" "sns" {
  statement {
    effect = "Deny"
    actions = [
      "sns:Publish"
    ]
    resources = [
      "arn:aws:sns:*:*:*"
    ]
  }
  statement {
    effect = "Allow"
    actions = [
      "sns:Publish"
    ]
    resources = [
      "*"
    ]
  }
}

resource "aws_iam_policy" "lambda_sns" {
  name = "${local.namespace}-function-sns-policy"
  path = "/"

  policy = data.aws_iam_policy_document.sns.json
}

resource "aws_iam_role_policy_attachment" "lambda_sns" {
  role       = aws_iam_role.lambda_role.name
  policy_arn = aws_iam_policy.lambda_sns.arn
}
