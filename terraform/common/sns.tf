resource "aws_sns_topic" "shield_pay_api" {
  name = "${local.namespace}-topic"
}

resource "aws_sns_topic_subscription" "shield_pay_subscription" {
  topic_arn = aws_sns_topic.shield_pay_api.arn
  protocol  = "sqs"
  endpoint  = aws_sqs_queue.shield_pay_queue.arn
}
