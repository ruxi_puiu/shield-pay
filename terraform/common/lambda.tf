data "archive_file" "lambda_zip" {
  type        = "zip"
  source_file = "../../dist/bundle.js"
  output_path = "lambda_function.zip"
}

resource "aws_lambda_function" "aws_lambda" {
  function_name    = "${local.namespace}-function"
  role             = aws_iam_role.lambda_role.arn
  handler          = "bundle.handler"
  runtime          = "nodejs12.x"
  filename         = "lambda_function.zip"
  source_code_hash = data.archive_file.lambda_zip.output_base64sha256
  timeout          = 10
  depends_on       = [aws_iam_role_policy_attachment.lambda_logs, aws_cloudwatch_log_group.lambda_log_group]
}

resource "aws_lambda_event_source_mapping" "lambda_source_mapping" {
  event_source_arn = aws_sqs_queue.shield_pay_queue.arn
  function_name    = aws_lambda_function.aws_lambda.arn
  batch_size       = 1
}
