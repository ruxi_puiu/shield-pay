
terraform {
  required_version = "~> 0.14.4"

}

provider "aws" {
  version = "~> 2.70.0"
  alias   = "eu-central-1"
  region  = "eu-central-1"
}

module "shield-pay-eu-central-1" {
  source = "../common"
  providers = {
    aws = aws.eu-central-1
  }
}